import React from 'react';
import card from './style/Card.module.css';
import color from './style/Colors.module.css';

function Movie(props) {
  const {movie} = props;

  return ( 
    <div className={card.card + " " + color.gray} key={movie.episode_id}>
      <div className={card.card_header}>
        <h5>{movie.title}</h5>
        <p>{movie.opening_crawl}</p>
      </div>
      <ul className={card.card_body}>
        <li>Directeur {movie.director}</li>
        <li>Producteur {movie.producer}</li>
        <li>Sortie le {movie.release_date}</li>
        <li><a href={movie.url}>Recupérer le site</a></li>
      </ul>
    </div>
  )
}

export default Movie;
