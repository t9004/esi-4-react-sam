import React from 'react';
import Movie from './Movie';

function MoviesList(props) {
  const {moviesList} = props;

  if (!moviesList || moviesList.length === 0) {
    console.log("No movies : ", moviesList)
    return ( <p>Cliquez sur le bouton pour afficher la liste des films</p> )
  } else {
    return ( 
      moviesList.map( movie => <Movie movie={movie} /> ) 
    )
  }
}

export default MoviesList;
