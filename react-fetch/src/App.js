import React, {useState} from 'react';
import MoviesList from './MoviesList';

function App() {

  const [moviesList, setMoviesList] = useState([])

  const callApiFilmHandler = () => {
    fetch('https://swapi.dev/api/films/').then((response) => response.json())
    .then(data => {
      console.log(data);
      setMoviesList(data.results);
    })
  }

  return (
    <div>
      <button onClick={callApiFilmHandler}>Afficher les films</button>
      <MoviesList moviesList={moviesList}/>
    </div>
  );
}

export default App;
