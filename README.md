# ESI 4 REACT SAM

## Présentation

J'ai un parcours assez basique : 
- Bac S SI sans savoir vraiment ce qui m'intéressait, et surtout parce que les profs me poussaient à le faire. Je n’ai jamais été passionné par les cours, mais j'aime beaucoup les sciences surtout quand je peux voir l'utilisation derrière... ça ne m'empêche pas d'être quand même moyen dans ce domaine.
- BTS SN IR en continu, j'en ai eu marre de la théorie et je voulais voir un peu plus un domaine qui m'intéressais : l'informatique, mais surtout la programmation, car ça me passionnait de pouvoir créer tout ce que l'on veut... premier de promo et j'ai acquis les bases de la POO + de la logique algorithmique, mais jamais de vraiment projet suivi et poussé.
- BAC +3 à ISITECH en alternance. J'en ai eu marre cette fois du cursus continu + taff à côté pour vivre dans des boulots mal payés. Je voulais me rapprocher du monde de l'entreprise après une expérience de 6 semaines à AIRBUS pendant le BTS. Changement de ville, de vie, j'ai enfin commencé à être payé pour ce que j'aime faire. Je me considère vraiment développeur à partir de ce moment.
- Cursus ESI

## Prise de notes
- Introduction à react  [ici](./doc-cours/presentation-react.md)
- J'avais du mal à comprendre la [destructuration](./doc-cours/dstructuration.md) mais maintenant c'est bien acquis.
- Notes sur le [spread operator](./doc-cours/spread-operator.md). Je connaissais l'utilisation;, mais je ne savais pas qu'il s'appelait comme ça.
- [Notes diverses](./doc-cours/divers.md)
- Voici ce que j'ai essayé, mais n'y suis pas arrivé : [ici](./doc-cours/fail.md)
- Mini projet react d'introduction dans le dossier /react-fetch

## Ce que j'ai appris
J'ai eu une petite expérience de React assez basique après un premier projet scolaire. Je n'ai jamais vraiment poussé la chose vu que très peu de temps avec le début de vie de salarié.
Grâce au monde de l'entreprise, j'ai pu évoluer assez vite dans les langages du web.

Avec cette semaine j'ai pu approfondir mes connaissances dans React surtout par rapport à son fonctionnement "bas niveau" avec sa gestion de la mémoire, son cycle de vie etc...

Également j'ai pu mieux découvrir l'utilité de coder avec les fonctions et non des classes. C'est beaucoup plus simple et rapide à déclarer et gérer grâce surtout avec les hooks.

J'ai déjà eu des connaissances dans la logique de store, car on a refait toute l'application mobile en flutter au travail avec mobx. Je ne connaissais pas le useContext de React je pensais qu'il n'y avait pas de gestion d'état aussi facile et natif à React.

J'ai découvert également ChakraUI qui nous a été "imposé" par tirage au sort. J'ai beaucoup apprécié travailler avec cette librairie, je ne connaissais ce monde de librairie "composant CSS". J'ai surtout utilisé bootstrap au travail et un peu de TailWindCss personnellement. Chakra est vraiment simple de mise en place, pas de configuration à faire et une documentation bien fournie. Je me sentais quand même perdu quand je devais aller dans les détails de manipulation CSS, je fais très peu de design, mais beaucoup de logique "front" et "back".