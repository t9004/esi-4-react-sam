## **Basics**
React est une librairie qui fait fonctionner une SPA (Single Page Application)

React s'insère dans une page html (/public/index.html) et manipule le DOM (avec la dépendance react-dom)

Le point d'entrée de l'application react est dans fichier src/index.js.

React utilise le JSX -> c'est le html mais en js pour permettre d'utiliser des fonctions de js directement dans le html.

Pour écrire du js dans du jsx on doit utiliser les accolades `{//ici du js}`

Pour lancer l'application on utilise la commande `npm start` dans le CLI. Cette commande va lancer le script dans le package.json  `"scripts": {
    "start": "react-scripts start"} `

React permet d'afficher des données. C'est son usage de base.

`() => {} // c'est une fonction anonyme`

## **Hooks et fonctions handler**

Un hook est une fonction qui commence par "use" et mis à disposition par react.
Les hook permettent de s'accrocher à des fonctions de react (voir cycle de vie d'un composant)

` const [myVar, setMyVar] = useState("Hello World");`

Dans un Handler (par exemple `onClickHandler = () => {}`) pour récupérer le dernier élément de la file d'attente de react pour une valeur on utilie "prevState" :
```js
const onClickHandler = () => {
    setMyVar((prevState) = () => {
        return prevState + 'update';
    })
}
```
## **Cycle de vie d'un composant**
Avec react on va utiliser de la programmation fonctionnelle majoritairement.

- componentDidMount, componentWillUpdate, componentWillUnmount sont remplacées par le useEffect

Le useEffect est de placer les effets de bord.

## **Récupérer les properties entre 2 composants**

// Dans un autre module :
```js
import Test from "./Test"
<Test myProp='hello' />
```

## Référence

Dans une action onclick par exemple, on lui donne une fonction handler sans les () car react s'occupe de passer une référence (adresse mémoire) au handler.
