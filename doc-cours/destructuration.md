### Déstructuration
#### Avec un objet
```
const myObject = {
    toto : 'toto'
}
const { toto } = myObject; // récupérer la variable "toto" dans un objet "myObject"
```

#### Avec un tableau
const myArray = [1, 2, 3];
// Peu importe le nom que l'on défini aux valeurs de myArray, le sens de dé
const [one, two, three] = myArray;