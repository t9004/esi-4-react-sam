## Reducer
On vient utiliser useReducer quand un useState devient trop complexe à gérer

Le reducer est une fonction pure donc on est capable de prédire le résultat de sortie.

Le reducer va mettre à jour le state, il doit recevoir toutes les données qu'il va modifier. Il ne peut pas recevoir des données extérieures.

exemple de store : 
```js
const initialState = {
    lastName: 'dijndsiqioj',
    name : 'toto',
    age : 42,
    status:{
        isOnline : true,
        lastLogin : '2020-01-01'
    }
}
const myReducer = (state,action) => {
    switch (action.type)  {
        case 'updateAge' :
            return {
                ...state
                age: payload
            }
        default:
            break;
    }
}
const [state, dispatch] = useReducer(myReducer,initialState)

dispatch({type:'UPDATE_AGE', payload:'43'})


```