### Spread Operator -> "..."
// permet de récupérer tout le reste des valeurs du myHttpResponse
// Ici price va récupérer la valeur de price dans l'objet myHttpResponse et ...rest tout le reste.
const {price, ...rest} = myHttpResponse;
